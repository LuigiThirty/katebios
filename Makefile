# I have my Dosbox sset up so that my mame\roms\ibm5150a directory is
# mounted as G: for quick testing.

MAMEDIR = G:\

all: 62X0820.U27 62X0821.U47
	copy $^ $(MAMEDIR)

62X0820.U27: ATBIOSV3.BIN
	objcopy --interleave=2 --byte=0 -I binary -O binary $< $@
	
62X0821.U47: ATBIOSV3.BIN
	objcopy --interleave=2 --byte=1 -I binary -O binary $< $@

ATBIOSV3.BIN: ATBIOS.OBJ
	debug < exe2bin

ATBIOS.OBJ: BIOS.OBJ BIOS1.OBJ BIOS2.OBJ DISK.OBJ DSKETTE.OBJ FILL.OBJ \
			KYBD.OBJ ORGS.OBJ PRT.OBJ RS232.OBJ TEST1.OBJ TEST2.OBJ TEST3.OBJ \
			TEST4.OBJ TEST5.OBJ TEST6.OBJ VIDEO1.OBJ KATE.OBJ
	link @atlink
	
%.OBJ: %.ASM
	masm $<\;
