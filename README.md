I do my builds in Dosbox. You'll need IBM MASM 2.0 and DJGPP's GNU Make in order to build the BIOS.
In the Makefile is a MAMEDIR variable. Set this to your output folder, or mount a directory in Dosbox
for your builds to be copied to.